﻿if (typeof page === 'undefined') {
    page = {};
}

(function () {
    var ToQueryParams = function (obj) {
        var str = [];

        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    };


    page.api = {
        getProductsInCategory: function (categoryId, pagingInfo) {
            var queryParams = ToQueryParams(pagingInfo);

            return fetch('/api/categories/' + categoryId + '/products?' + queryParams)
                .then(function (response) {
                    return response.json();
                });
        },
        getCategories: function () {
            return fetch('/api/categories')
                .then(function (response) {
                    return response.json();
                });
        },
        getFeaturedProducts: function (pagingInfo) {
            var queryParams = ToQueryParams(pagingInfo);

            return fetch('/api/products/featured?' + queryParams)
                .then(function (response) {
                    return response.json();
                });
        }
    };
})();