﻿using MMT.Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.Shop.Repository
{
    public interface ICategoryRepository : IRepository<Category,int>
    {
    }
}
