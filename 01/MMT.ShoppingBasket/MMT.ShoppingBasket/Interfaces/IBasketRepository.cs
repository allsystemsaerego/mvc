﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.ShoppingBasket.Interfaces
{
    public interface IBasketRepository
    {
        int Add(IProduct product);
        int Remove(IProduct product);       
    }
}
