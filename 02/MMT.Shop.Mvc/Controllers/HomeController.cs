﻿using MMT.Shop.Models;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MMT.Shop.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}