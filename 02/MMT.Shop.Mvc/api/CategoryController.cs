﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using MMT.Shop.Models;
using MMT.Shop.Repository;

namespace MMT.Shop.Mvc.api
{
    public class CategoryController : ApiController
    {
        private static ICategoryRepository _categoryRepository;

        public CategoryController(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        // GET api/<controller>
        public IEnumerable<Category> Get()
        {
            return _categoryRepository.FetchAll();
        }

        // GET api/<controller>/5
        public Category Get(int id)
        {
            var category = _categoryRepository.Fetch(id);

            if (category == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return category;
        }       
    }
}