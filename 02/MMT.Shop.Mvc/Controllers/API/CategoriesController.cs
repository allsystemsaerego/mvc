﻿using MMT.Shop.Models;
using MMT.Shop.Mvc.Models;
using MMT.Shop.Repository;
using MMT.Shop.Repository.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MMT.Shop.Mvc.Controllers.API
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        private ICategoryRepository _categoryRepository;
        private IProductRepository _productRepository;

        public CategoriesController(ICategoryRepository categoryRepository, IProductRepository productRepository)
        {
            if (categoryRepository == null)
            {
                throw new ArgumentNullException(nameof(categoryRepository));
            }

            _categoryRepository = categoryRepository;

            if (productRepository == null)
            {
                throw new ArgumentNullException(nameof(productRepository));
            }

            _productRepository = productRepository;
        }

        [HttpGet]
        [Route("")]
        public IEnumerable<Category> Get()
        {
            return _categoryRepository.FetchAll();
        }

        [HttpGet]
        [Route("{id}/products")]
        public Results<Product> GetProductsInCategory(int id, [FromUri] PagingInfo pageInfo)
        {
            var categoryExists = _categoryRepository.Exists(id);

            if (!categoryExists)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var products = _productRepository.FetchProductsInCategory(id, pageInfo);

            var productCount = _productRepository.GetProductsInCategoryCount(id);

            return new Results<Product>(products, productCount);
        }

        [HttpGet]
        [Route("{id}")]
        public Category Get(int id)
        {
            var category = _categoryRepository.Fetch(id);

            if (category == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return category;
        }
    }
}