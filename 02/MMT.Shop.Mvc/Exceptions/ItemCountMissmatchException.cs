﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MMT.Shop.Mvc.Exceptions
{
    public class ItemCountMissmatchException : Exception
    {
        public ItemCountMissmatchException(string message)
            : base(message)
        {

        }
    }
}