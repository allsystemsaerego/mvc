﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MMT.Shop.Mvc.Exceptions;

namespace MMT.Shop.Mvc.Models
{
    public class Results<T>
    {
        public List<T> Items { get; private set; }
        public int TotalCount { get; private set; }

        public Results(IEnumerable<T> items, int totalItemCount)
        {
            Items = items.ToList();
            TotalCount = totalItemCount;

            if(Items.Count > totalItemCount)
            {
                throw new ItemCountMissmatchException("Results collection size is greater than the total results");
            }
        }
    }
}
