﻿using System.Web.Http;
using MMT.Shop.Mvc.Classes;

namespace MMT.Shop.Mvc
{
    public class ApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.DependencyResolver = new UnityResolver(UnityConfig.Container);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );     
        }
    }
}