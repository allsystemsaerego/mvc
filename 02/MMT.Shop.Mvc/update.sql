﻿UPDATE Product
SET Product.IsFeatured = 1
WHERE Product.SKU Like '[1-3]____'

Go

CREATE OR ALTER VIEW [dbo].[vw_ProductCategories]
	AS 
	
	SELECT 
		CAT.Id AS CategoryId,
		CAT.Name AS CategoryName,
		PR.Id AS ProductId,
		PR.Name AS ProductName,
		PR.Price AS ProductPrice,
		PR.IsFeatured as ProductIsFeatured,
		PR.SKU AS ProductSKU
	FROM Category AS CAT
	CROSS JOIN Product AS PR
	WHERE CAT.Name = 'Home' AND PR.SKU LIKE '1____' 
	OR CAT.Name = 'Garden' AND PR.SKU LIKE '2____' 
	OR CAT.Name = 'Electronics' AND PR.SKU LIKE '3____' 
	OR CAT.Name = 'Fitness' AND PR.SKU LIKE '4____' 
	OR CAT.Name = 'Toys' AND PR.SKU LIKE '5____'

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Product_GetProductsInCategoryCount]
	@categoryId int = 0
AS
	SELECT COUNT(*) from Product PR
	JOIN vw_ProductCategories PC
	ON PR.Id = PC.ProductId
	WHERE PC.CategoryId = @categoryId

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Category_Fetch]
	@id int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT Id, Name FROM Category WHERE Category.Id = @id
END

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Category_Exists]
	@id int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT CAST(COUNT(*) AS BIT) FROM Category WHERE Category.Id = @id
END

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Product_FetchProductsInCategory]
	@categoryId int,
	@skip int = 0,
	@take int = 4
AS
	SELECT PR.Id, PR.Name, PR.Description, PR.IsFeatured, PR.Price, PR.SKU from Product PR
	JOIN vw_ProductCategories PC
	ON PR.Id = PC.ProductId
	WHERE PC.CategoryId = @categoryId
	ORDER BY PR.Name
	OFFSET @skip ROWS
	FETCH NEXT @take ROWS ONLY;

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Product_FetchFeatured]
	@skip int = 0,
	@take int = 4
AS
	SELECT Id, Name, Description, IsFeatured, Price, SKU
	FROM Product
	WHERE Product.IsFeatured = 1
	ORDER BY Product.Name
	OFFSET @skip ROWS
	FETCH NEXT @take ROWS ONLY;

GO

CREATE OR ALTER PROCEDURE [dbo].[usp_Product_GetFeaturedCount]
AS
	SELECT COUNT(*) FROM Product
	WHERE Product.IsFeatured = 1