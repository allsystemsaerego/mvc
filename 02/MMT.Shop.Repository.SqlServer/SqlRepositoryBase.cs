﻿
namespace MMT.Shop.Repository.SqlServer
{
    public abstract class SqlRepositoryBase
    {
        /// <summary>
        /// This is the name of the connection string to use for data access.        
        /// </summary>
        internal const string CONNECTION_STRING_NAME = "MMT.Shop";
    }
}
