﻿using MMT.ShoppingBasket.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.ShoppingBasket
{
   
    public class ShoppingBasket
    {
        private IBasketRepository _basketRepository;
        private IUser _user;
        public bool IsBasketInitiated
        {
            get;
            private set;
        }

        public ShoppingBasket(IBasketRepository basketRepository, IUser user)
        {
            if (basketRepository == null)
                throw new ArgumentNullException(nameof(basketRepository));

            if (user == null)
                throw new ArgumentNullException(nameof(user));

            _basketRepository = basketRepository;
            _user = user;

            IsBasketInitiated = true;
        }

        public int Add(IProduct product)
        {
            return _basketRepository.Add(product);
        }

        public int Remove(IProduct product)
        {
            return _basketRepository.Remove(product);
        }
    }
}
