﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.Shop.Repository
{
    public interface IRepository<T,TKey> where T:class
    {
        //Fetch Routines
        T Fetch(TKey key);
        IEnumerable<T> FetchAll();

        //CRUD
        void Add(T model);

        void Remove(T model);
        void Remove(TKey key);
        bool Exists(int id);
    }
}
