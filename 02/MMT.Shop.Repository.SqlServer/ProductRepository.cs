﻿using MMT.Core.Data;
using MMT.Shop.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MMT.Shop.Repository.SqlServer
{
    public class ProductRepository : SqlRepositoryBase, IProductRepository
    {
        public ProductRepository()
        {

        }

        public bool Exists(int key)
        {
            throw new NotImplementedException();
        }

        public void Add(Product model)
        {
            throw new NotImplementedException();
        }

        public Product Fetch(int key)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> FetchAll()
        {
            throw new NotImplementedException();
        }

        public int GetFeaturedProductsCount()
        {
            var featuredProductsCount = DataAccess.GetScalar(CONNECTION_STRING_NAME, "usp_Product_GetFeaturedCount", CommandType.StoredProcedure);

            return (int)featuredProductsCount;
        }

        public int GetProductsInCategoryCount(int categoryId)
        {
            var categoryParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "categoryId", categoryId);

            var productsInCategoryCount = DataAccess.GetScalar(CONNECTION_STRING_NAME, "usp_Product_GetProductsInCategoryCount", CommandType.StoredProcedure, new[] { categoryParam });
            
            return (int)productsInCategoryCount;
        }

        public IEnumerable<Product> FetchProductsInCategory(int categoryId, PagingInfo pagingInfo)
        {
            if (pagingInfo == null)
            {
                throw new ArgumentNullException(nameof(pagingInfo));
            }            

            var categoryParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "categoryId", categoryId);

            var skipParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "skip", pagingInfo.GetSkip());

            var takeParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "take", pagingInfo.PageSize);

            var dataTable = DataAccess.GetDataTable(CONNECTION_STRING_NAME, "usp_Product_FetchProductsInCategory", CommandType.StoredProcedure, new[] { categoryParam, skipParam, takeParam });

            var products = dataTable
                .AsEnumerable()
                .Select(ConvertToProduct)
                .ToList();

            return products;
        }


        public IEnumerable<Product> FetchFeaturedProducts(PagingInfo pagingInfo)
        {
            if (pagingInfo == null)
            {
                throw new ArgumentNullException(nameof(pagingInfo));
            }

            var skipParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "skip", pagingInfo.GetSkip());

            var takeParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "take", pagingInfo.PageSize);
            
            var dataTable = DataAccess.GetDataTable(CONNECTION_STRING_NAME, "usp_Product_FetchFeatured", CommandType.StoredProcedure, new[] { skipParam, takeParam });

            var products = dataTable
                .AsEnumerable()
                .Select(ConvertToProduct)
                .ToList();

            return products;
        }

        public void Remove(Product model)
        {
            throw new NotImplementedException();
        }

        public void Remove(int key)
        {
            throw new NotImplementedException();
        }

        private Product ConvertToProduct(DataRow row)
        {
            return new Product()
            {
                Id = SafeDataRowReader.ReadRow<int>(row, "Id", 0),
                SKU = SafeDataRowReader.ReadRow<string>(row, "SKU", string.Empty),
                Name = SafeDataRowReader.ReadRow<string>(row, "Name", string.Empty),
                Description = SafeDataRowReader.ReadRow<string>(row, "Description", string.Empty),
                Price = SafeDataRowReader.ReadRow<decimal>(row, "Price", (decimal)0),
                IsFeatured = SafeDataRowReader.ReadRow<bool>(row, "IsFeatured", true),
            };
        }
    }
}
