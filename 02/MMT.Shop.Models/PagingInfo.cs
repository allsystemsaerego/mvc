﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.Shop.Models
{
    public class PagingInfo
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; } = 4;

        public int GetSkip()
        {
            return (PageNo - 1) * PageSize;
        }
    }
}
