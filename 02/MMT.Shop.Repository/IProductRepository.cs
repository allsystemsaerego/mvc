﻿using MMT.Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MMT.Shop.Repository
{
    public interface IProductRepository:IRepository<Product,int>
    {
        int GetProductsInCategoryCount(int categoryId);
        int GetFeaturedProductsCount();
        IEnumerable<Product> FetchFeaturedProducts(PagingInfo pagingInfo);
        IEnumerable<Product> FetchProductsInCategory(int categoryId, PagingInfo pagingInfo);
    }
}
