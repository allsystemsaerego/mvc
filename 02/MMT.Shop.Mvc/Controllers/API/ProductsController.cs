﻿using MMT.Shop.Models;
using MMT.Shop.Mvc.Models;
using MMT.Shop.Repository;
using MMT.Shop.Repository.SqlServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MMT.Shop.Mvc.Controllers.API
{
    [RoutePrefix("api/products")]
    public class ProductsController : ApiController
    {
        private IProductRepository _productRepository;

        public ProductsController(IProductRepository productRepository)
        {
            if (productRepository == null)
            {
                throw new ArgumentNullException(nameof(productRepository));
            }

            _productRepository = productRepository;
        }
        
        [HttpGet]
        [Route("featured")]
        public Results<Product> GetFeatured([FromUri] PagingInfo pageInfo)
        {
            var products = _productRepository.FetchFeaturedProducts(pageInfo);

            var productCount = _productRepository.GetFeaturedProductsCount();

            return new Results<Product>(products, productCount);
        }
    }
}