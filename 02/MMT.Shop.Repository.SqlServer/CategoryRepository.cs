﻿using MMT.Shop.Models;
using MMT.Core.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MMT.Shop.Repository.SqlServer
{
    public class CategoryRepository : SqlRepositoryBase , ICategoryRepository
    {
        public CategoryRepository()
        {

        }

        public bool Exists(int key)
        {
            var categoryParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "id", key);

            var exists = (bool)DataAccess.GetScalar(CONNECTION_STRING_NAME, "usp_Category_Exists", CommandType.StoredProcedure, new[] { categoryParam });
            
            return exists;
        } 

        public Category Fetch(int key)
        {
            var categoryParam = DataAccess.CreateParameter(CONNECTION_STRING_NAME, "id", key);

            var dataTable = DataAccess.GetDataTable(CONNECTION_STRING_NAME, "usp_Category_Fetch", CommandType.StoredProcedure, new[] { categoryParam });

            var category = dataTable
                .AsEnumerable()
                .Select(ConvertToCategory)
                .FirstOrDefault();

            return category;
        }

        public IEnumerable<Category> FetchAll()
        {
            var dataTable = DataAccess.GetDataTable(CONNECTION_STRING_NAME, "usp_Category_FetchAll", CommandType.StoredProcedure);

            var categories = dataTable
                .AsEnumerable()
                .Select(ConvertToCategory)
                .ToList();
            
            return categories;            
        }

        public void Add(Category model)
        {
            throw new NotImplementedException();
        }

        public void Remove(Category model)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        private Category ConvertToCategory(DataRow row)
        {
            return new Category()
            {
                Id = SafeDataRowReader.ReadRow<int>(row, "Id", 0),
                Name = SafeDataRowReader.ReadRow<string>(row, "Name", string.Empty)
            };
        }
    }
}
