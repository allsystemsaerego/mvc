﻿using MMT.Shop.Models;
using MMT.Shop.Repository;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MMT.Shop.Mvc.Controllers
{
    [RoutePrefix("categories")]
    public class CategoriesController : Controller
    {
        private ICategoryRepository _categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
            if(categoryRepository == null)
            {
                throw new System.ArgumentNullException(nameof(categoryRepository));
            }

            _categoryRepository = categoryRepository; 
        }

        [Route("{id}/products")]
        public ActionResult Products(int id)
        {
            var category = _categoryRepository.Fetch(id);

            if (category == null)
                throw new HttpException((int)HttpStatusCode.NotFound, $"Category '{id}' could not be found");
            
            return View(category);
        }
    }
}