﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MMT.ShoppingBasket.Interfaces;
using MMT.ShoppingBasket.Exceptions;
using SimpleInjector;

namespace MMT.ShoppingBasket.Test
{
    [TestClass]
    public class ShoppingBasketTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShoppingasketTests_BasketRepositoryIsNull_ThrowsArgumentNullException()
        {
            var moqUser = new Mock<IUser>();
            var shoppingBasket = new ShoppingBasket(null, moqUser.Object);
        }
       
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ShoppingasketTests_UserIsNull_ThrowsArgumentNullException()
        {
            var moqBasketRepository = new Mock<IBasketRepository>();
            var shoppingBasket = new ShoppingBasket(moqBasketRepository.Object, null);
        }

        [TestMethod]
        public void ShippingBasketTest_UserAndBasketRepositoryNotNull_BasketIsInitialised()
        {
            var moqBasketRepository = new Mock<IBasketRepository>();
            var moqUser = new Mock<IUser>();

            var shoppingBasket = new ShoppingBasket(moqBasketRepository.Object, moqUser.Object);
            Assert.IsTrue(shoppingBasket.IsBasketInitiated);
        }
        
        [TestMethod]
        public void ShippingBasketTest_AddItemToBasketSuccess_RepositoryStatusCodeReturned()
        {
            var moqBasketRepository = new Mock<IBasketRepository>();
            var moqUser = new Mock<IUser>();
            var moqProduct = new Mock<IProduct>();
            
            var randomNumber = new Random().Next(1000);

            moqBasketRepository
                .Setup(x => x.Add(moqProduct.Object))
                .Returns(randomNumber);

            var shoppingBasket = new ShoppingBasket(moqBasketRepository.Object, moqUser.Object);
            Assert.IsTrue(shoppingBasket.IsBasketInitiated);

            var statusCode = shoppingBasket.Add(moqProduct.Object);

            Assert.AreEqual(randomNumber, statusCode);
        }

        [TestMethod]
        public void ShippingBasketTest_RemoveItemFromBasketSuccess_RepositoryStatusCodeReturned()
        {
            var moqBasketRepository = new Mock<IBasketRepository>();
            var moqUser = new Mock<IUser>();
            var moqProduct = new Mock<IProduct>();

            var randomNumber = new Random().Next(1000);

            moqBasketRepository
                .Setup(x => x.Remove(moqProduct.Object))
                .Returns(randomNumber);

            var shoppingBasket = new ShoppingBasket(moqBasketRepository.Object, moqUser.Object);
            Assert.IsTrue(shoppingBasket.IsBasketInitiated);

            var statusCode = shoppingBasket.Remove(moqProduct.Object);

            Assert.AreEqual(randomNumber, statusCode);
        }

        [TestMethod]
        [ExpectedException(typeof(BasketRepositoryException))]
        public void ShippingBasketTest_RemovingItemToBasketError_ThrowsBasketRepositoryException()
        {
            var moqBasketRepository = new Mock<IBasketRepository>();
            var moqUser = new Mock<IUser>();
            var moqProduct = new Mock<IProduct>();

            var exception = new BasketRepositoryException("Test error");

            moqBasketRepository
                .Setup(x => x.Remove(moqProduct.Object))
                .Throws(exception);

            var shoppingBasket = new ShoppingBasket(moqBasketRepository.Object, moqUser.Object);
            Assert.IsTrue(shoppingBasket.IsBasketInitiated);

            var statusCode = shoppingBasket.Remove(moqProduct.Object);
        }
        
        [TestMethod]
        public void ShippingBasketTest_DependencyInjectorExample_ShoppingBasketInitialisedFromContainer()
        {
            var diContext = new Container();
            diContext.RegisterSingleton<IBasketRepository>(() => new Mock<IBasketRepository>().Object);
            diContext.RegisterSingleton<IUser>(() => new Mock<IUser>().Object);
            diContext.Register<ShoppingBasket>();
            diContext.Verify();
            
            var shoppingBasket = diContext.GetInstance<ShoppingBasket>();
            Assert.IsTrue(shoppingBasket.IsBasketInitiated);
        }
    }
}
